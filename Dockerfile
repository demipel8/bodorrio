FROM ruby:2.4.1

ARG WORKDIR="/code"

WORKDIR $WORKDIR

COPY Gemfile $WORKDIR/Gemfile
COPY Gemfile.lock $WORKDIR/Gemfile.lock
RUN bundle install -j10

COPY . $WORKDIR/

EXPOSE 8080

CMD ["bundle", "exec", "puma", "config.ru", "-b", "tcp://0.0.0.0:8080"]
