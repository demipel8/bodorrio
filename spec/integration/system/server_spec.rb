# frozen_string_literal: true

require 'rack/test'
require 'server'
require 'json'

RSpec.describe 'Integration files' do
  include Rack::Test::Methods

  def app
    System::Server
  end

  before do
    WebMock.allow_net_connect!
  end

  after do
    WebMock.disable_net_connect!
  end

  describe 'root endpoint' do
    it 'returns html' do
      get '/'

      expect(last_response.ok?).to be
      expect(last_response.headers['Content-Type']).to eq('text/html;charset=utf-8')
      expect(last_response.headers['Content-Length'].to_i).to be > 0
    end
  end

  describe 'invitee endpoint' do
    it 'returns a lunch invitee its plan' do
      post '/invitee', '{ "name": "a_family_member" }', 'CONTENT_TYPE' => 'application/json'

      body = JSON.parse(last_response.body, symbolize_names: true)

      expect(body).to eq(lunch_invitee)
    end

    it 'returns a party invitee its plan' do
      post '/invitee', '{ "name": "a_friend" }', 'CONTENT_TYPE' => 'application/json'

      body = JSON.parse(last_response.body, symbolize_names: true)

      expect(body).to eq(party_invitee)
    end

    it 'returns a stranger its plan' do
      post '/invitee', '{ "name": "a_stranger" }', 'CONTENT_TYPE' => 'application/json'

      body = JSON.parse(last_response.body, symbolize_names: true)
      component = body[0]

      expect(component[:component]).to eq('not_an_invitee')
      expect(component[:data]).to have_key(:gif)
      expect(component[:data]).to have_key(:description)
    end

    def lunch_invitee
      [
        {
          component: 'event',
          data: {
            name: 'ceremony',
            description: 'A traditional unicorn ceremony dude',
            hour: '08:00',
            time: '2018-05-15 08:00:00 UTC',
            labels: %w[party events lunch]
          }
        },
        {
          component: 'event',
          data: {
            name: 'lunch',
            description: "Some lobster and cake\n\nWhat a party!",
            hour: '12:00',
            time: '2018-05-16 12:00:00 UTC',
            labels: %w[lunch events]
          }
        },
        {
          component: 'event',
          data: {
            name: 'party',
            description: 'Drinks and djs',
            hour: '17:00',
            time: '2018-05-16 17:00:00 UTC',
            labels: %w[lunch party events]
          }
        },
        {
          component: 'event',
          data: {
            name: 'after party',
            description: '',
            hour: '01:00',
            time: '2018-05-17 01:00:00 UTC',
            labels: %w[lunch party events]
          }
        },
        {
          component: 'localization',
          data: {
            name: 'bariloche',
            google_maps_embed_url: 'https://www.google.com/maps/embed/v1/place?key=AIzaSyChNJ6WE-X43sC241_DpxvQEW0hh8xpOoI&q=bariloche'
          }
        },
        {
          component: 'localization',
          data:
          {
            name: 'Santa Coloma de Queralt',
            google_maps_embed_url: 'https://www.google.com/maps/embed/v1/place?key=AIzaSyChNJ6WE-X43sC241_DpxvQEW0hh8xpOoI&q=Santa+Coloma+de+Queralt'
          }
        },
        {
          component: 'survey',
          data: {
            id: '5aa9a1c2d31f6ce1b9727f79',
            name: 'Beverages survey',
            question: 'What do you drink?',
            options: {
              rum: false,
              gin: true,
              vodka: false,
              other: true
            }
          }
        },
        {
          component: 'survey',
          data: {
            id: '5ac29252589f819ba66e09ca',
            name: 'Costume survey',
            question: 'What costume are you wearing a the party?',
            options: {
              Tiger: false,
              Dragon: false,
              Panda: false
            }
          }
        },
        {
          component: 'competition',
          data: {
            name: 'Drinking',
            options: [
              { name: 'Gin', total: '50', done: '5' },
              { name: 'Rum', total: '50', done: '4' }
            ]
          }
        }
      ]
    end

    def party_invitee
      [
        {
          component: 'event',
          data: {
            name: 'ceremony',
            description: 'A traditional unicorn ceremony dude',
            hour: '08:00',
            time: '2018-05-15 08:00:00 UTC',
            labels: %w[party events lunch]
          }
        },
        {
          component: 'event',
          data: {
            name: 'party',
            description: 'Drinks and djs',
            hour: '17:00',
            time: '2018-05-16 17:00:00 UTC',
            labels: %w[lunch party events]
          }
        },
        {
          component: 'event',
          data: {
            name: 'after party',
            description: '',
            hour: '01:00',
            time: '2018-05-17 01:00:00 UTC',
            labels: %w[lunch party events]
          }
        },
        {
          component: 'localization',
          data: {
            name: 'bariloche',
            google_maps_embed_url: 'https://www.google.com/maps/embed/v1/place?key=AIzaSyChNJ6WE-X43sC241_DpxvQEW0hh8xpOoI&q=bariloche'
          }
        },
        {
          component: 'localization',
          data:
          {
            name: 'Santa Coloma de Queralt',
            google_maps_embed_url: 'https://www.google.com/maps/embed/v1/place?key=AIzaSyChNJ6WE-X43sC241_DpxvQEW0hh8xpOoI&q=Santa+Coloma+de+Queralt'
          }
        },
        {
          component: 'survey',
          data: {
            id: '5aa9a1c2d31f6ce1b9727f79',
            name: 'Beverages survey',
            question: 'What do you drink?',
            options: {
              rum: true,
              gin: false,
              vodka: false,
              other: true
            }
          }
        },
        {
          component: 'survey',
          data: {
            id: '5ac29252589f819ba66e09ca',
            name: 'Costume survey',
            question: 'What costume are you wearing a the party?',
            options: {
              Tiger: false,
              Dragon: false,
              Panda: false
            }
          }
        },
        {
          component: 'competition',
          data: {
            name: 'Drinking',
            options: [
              { name: 'Gin', total: '50', done: '5' },
              { name: 'Rum', total: '50', done: '4' }
            ]
          }
        }
      ]
    end
  end
  describe 'survey endpoint' do
    it 'adds a user to a survey option' do
      post '/survey', '{ "name": "a_different_member", "survey": "5aa9a1c2d31f6ce1b9727f79", "option": "rum", "checked": true }', 'CONTENT_TYPE' => 'application/json'

      expect(last_response.status).to be 200
    end

    it 'deletes a user from a survey option' do
      post '/survey', '{ "name": "a_different_member", "survey": "5aa9a1c2d31f6ce1b9727f79", "option": "rum", "checked": true }', 'CONTENT_TYPE' => 'application/json'
      post '/survey', '{ "name": "a_different_member", "survey": "5aa9a1c2d31f6ce1b9727f79", "option": "rum", "checked": false }', 'CONTENT_TYPE' => 'application/json'

      expect(last_response.status).to be 200
    end
  end
end
