# frozen_string_literal: true

module FIXTURES
  def lunch_list
    JSON.parse(lunch_list_json, symbolize_names: true)
  end

  def party_list
    JSON.parse(party_list_json, symbolize_names: true)
  end

  def localization_description
    JSON.parse(localization_description_json, symbolize_names: true)
  end

  def description
    JSON.parse(description_json, symbolize_names: true)
  end

  def due_date
    JSON.parse(due_date_json, symbolize_names: true)
  end

  def name
    JSON.parse(name_json, symbolize_names: true)
  end

  def labels
    JSON.parse(labels_json, symbolize_names: true)
  end

  def raw_card
    {
      id: '5ab3fb08900b981e8ebd7d7',
      desc: 'Santa Coloma de Queralt',
      due: '2018-06-08T11:30:00.000Z',
      name: 'name',
      labels: [],
      checklists: [{
        id: '5ab55510a400f2e6dc97259f',
        name: 'rum',
        checkItems: [
          {
            state: 'incomplete',
            idChecklist: '5ab55510a400f2e6dc97259f',
            id: '5ab58efdaeae9f82335b2ba6',
            name: 'A_friend',
            nameData: {
              emoji: {}
            },
            pos: 16_502
          },
          {
            state: 'incomplete',
            idChecklist: '5ab55510a400f2e6dc97259f',
            id: '5ac2942810d5396207483dc4',
            name: 'Lol',
            nameData: nil,
            pos: 32_886
          },
          {
            state: 'incomplete',
            idChecklist: '5ab55510a400f2e6dc97259f',
            id: '5acfb48a2c4cdc532ceb440f',
            name: 'A_different_member',
            nameData: nil,
            pos: 49_270
          }
        ]
      }, {
        id: '5ab555146b345ecf1811b0a0',
        name: 'gin',
        checkItems: [
          {
            state: 'incomplete',
            idChecklist: '5ab555146b345ecf1811b0a0',
            id: '5ab593fb636920866d930f4d',
            name: 'A_family_member',
            nameData: {
              emoji: {}
            },
            pos: 16_624
          }
        ]
      }, {
        id: '5ab555185547c4012a775c60',
        name: 'vodka',
        checkItems: []
      }]
    }
  end

  def card
    {
      id: '5ab3fb08900b981e8ebd7d7',
      desc: 'Santa Coloma de Queralt',
      due: nil,
      name: 'name',
      labels: [],
      checklists: []
    }
  end

  def localization_description_json
    <<-JSON
    {
      "_value":"bariloche"
    }
    JSON
  end

  def description_json
    <<-JSON
    {
      "_value":"it is comming\\n yes it is"
    }
    JSON
  end

  def due_date_json
    <<-JSON
    {
      "_value":"2018-06-08T11:30:00.000Z"
    }
    JSON
  end

  def name_json
    <<-JSON
    {
      "_value":"a name"
    }
    JSON
  end

  def labels_json
    <<-JSON
    [
      {
        "id":"8g3yf39ryfg93f",
        "idBoard":"ob3jbf3uirfb93",
        "name":"lunch",
        "color":"green",
        "uses":3
        },
        {
          "id":"fh9b3ub3rv3",
          "idBoard":"ou3efb3oebf3ef",
          "name":"party",
          "color":"yellow",
          "uses":2
        }
      ]
    JSON
  end

  def localization_component
    {
      component: 'localization',
      data: {
        name: 'bariloche',
        google_maps_embed_url: 'an_url'
      }
    }
  end

  def event_component
    {
      component: 'event',
      data: {
        description: 'it is comming',
        hour: '11:30',
        time: '2018-06-08 11:30:00 UTC',
        labels: %w[lunch party]
      }
    }
  end

  def lunch_event_component
    {
      component: 'event',
      data: {
        description: 'it is comming',
        hour: '11:30',
        time: '2018-06-08 11:30:00 UTC',
        labels: ['lunch']
      }
    }
  end

  def survey_component
    {
      component: 'survey',
      data: {
        id: 'survey_card_id',
        name: 'drink survey',
        question: 'What do you drink?',
        options: {
          rum: true,
          gin: false,
          vodka: false,
          other: true
        }
      }
    }
  end

  def not_an_invitee_component
    {
      component: 'not_an_invitee',
      data: {
        description: 'who are you?',
        gif: 'a_gif_url'
      }
    }
  end

  def lunch_list_json
    <<-JSON
    [
      {
        "id": "5a8c43dec68829d07g6ea6e",
        "checkItems": [
          {
            "idChecklist": "5ad4bd5dec63729d0736e16e",
            "id": "5a8c90000068829d1956ea6f",
            "name": "a_familiar"
          },
          {
            "idChecklist": "5ad6bd5dec73729d07361234",
            "id": "5a8c9d5dt5b8g29d07364321",
            "name": "another_familiar"
          }
        ]
      },
      {
        "id": "45679d77f1275cdaf15y5672",
        "checkItems": [
          {
            "idChecklist": "d3f40d77f1275cdaf3815472",
            "id": "7e8c9d7ght575cdafgdj54rt",
            "name": "a_familiar_from_another_list"
          }
        ]
      }
    ]
    JSON
  end

  def party_list_json
    <<-JSON
    [
      {
        "id": "5a8c43dec68829d07g6ea6e",
        "checkItems": [
          {
            "idChecklist": "5ad4bd5dec63729d0736e16e",
            "id": "5a8c90000068829d1956ea6f",
            "name": "a_friend"
          },
          {
            "idChecklist": "5ad6bd5dec73729d07361234",
            "id": "5a8c9d5dt5b8g29d07364321",
            "name": "another_friend"
          }
        ]
      }
    ]
    JSON
  end

  module_function :lunch_list, :party_list, :lunch_list_json, :party_list_json,
    :localization_description, :localization_description_json, :localization_component,
    :due_date_json, :due_date, :description_json, :description, :labels,
    :name_json, :name, :not_an_invitee_component, :lunch_event_component,
    :event_component, :labels_json, :survey_component, :card, :raw_card
end
