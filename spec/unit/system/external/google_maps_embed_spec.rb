# frozen_string_literal: true

require 'external/google_maps_embed'

RSpec.describe 'Google maps embed' do
  it 'get the url for a place' do
    ENV['GOOGLE_MAPS_EMBED_API_KEY'] = 'an_api_key'
    ENV['GOOGLE_MAPS_EMBED_BASE_URL'] = 'http://google_maps_embed.com'

    google_maps_embed = GoogleMapsEmbed.new

    response = google_maps_embed.url_for('a place')

    expect(response).to eq(google_maps_embed_url)
  end

  def google_maps_embed_url
    'http://google_maps_embed.com/place?key=an_api_key&q=a+place'
  end
end
