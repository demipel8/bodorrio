# frozen_string_literal: true

require 'external/trello'
require 'json'

RSpec.describe 'trello' do
  before do
    ENV['TRELLO_BASE_URL'] = 'https://trello_api.com'
    ENV['TRELLO_KEY'] = 'a_key'
    ENV['TRELLO_TOKEN'] = 'a_token'

    @trello = Trello.new
  end

  it 'get checklists elements in a card' do
    stub_request(:get, 'https://trello_api.com/cards/a_card_id/checklists?key=a_key&token=a_token&checkItem_fields=name&fields=checkItems,name')
      .to_return(status: 200, body: FIXTURES.lunch_list_json)

    response = @trello.checklists_in_card('a_card_id')

    expect(response).to eq(FIXTURES.lunch_list)
  end

  it 'get the description in a card' do
    stub_request(:get, 'https://trello_api.com/cards/a_card_id/desc?key=a_key&token=a_token')
      .to_return(status: 200, body: FIXTURES.localization_description_json)

    response = @trello.description_in_card('a_card_id')

    expect(response).to eq(FIXTURES.localization_description)
  end

  it 'get the due date in a card' do
    stub_request(:get, 'https://trello_api.com/cards/a_card_id/due?key=a_key&token=a_token')
      .to_return(status: 200, body: FIXTURES.due_date_json)

    response = @trello.due_date_in_card('a_card_id')

    expect(response).to eq(FIXTURES.due_date)
  end

  it 'get the name of a card' do
    stub_request(:get, 'https://trello_api.com/cards/a_card_id/name?key=a_key&token=a_token')
      .to_return(status: 200, body: FIXTURES.name_json)

    response = @trello.name_in_card('a_card_id')

    expect(response).to eq(FIXTURES.name)
  end

  it 'get the labels of a card' do
    stub_request(:get, 'https://trello_api.com/cards/a_card_id/labels?key=a_key&token=a_token')
      .to_return(status: 200, body: FIXTURES.labels_json)

    response = @trello.labels_in_card('a_card_id')

    expect(response).to eq(FIXTURES.labels)
  end

  it 'adds an option to a checklist' do
    stub = stub_request(:post, 'https://trello_api.com/checklists/a_checklist_id/checkItems?name=a_name&key=a_key&token=a_token')
    stub.to_return(status: 200, body: {}.to_json)

    @trello.add_item_to_checklists('a_checklist_id', 'a_name')

    expect(stub).to have_been_requested
  end

  it 'deletes an option to a checklist' do
    stub = stub_request(:delete, 'https://trello_api.com/checklists/a_checklist_id/checkItems/an_item_id?key=a_key&token=a_token')
    stub.to_return(status: 200, body: {}.to_json)

    @trello.delete_item_from_checklists('a_checklist_id', 'an_item_id')

    expect(stub).to have_been_requested
  end

  it 'gets all required elements in a card' do
    stub_request(:get, 'https://trello_api.com/cards/a_card_id?key=a_key&token=a_token&fields=desc%2Cdue%2Cname%2Clabels&attachments=false&checklists=all&checklist_fields=name')
      .to_return(status: 200, body: JSON.dump(card))

    response = @trello.card('a_card_id')

    expect(response).to eq(card)
  end

  it 'gets all cards in a board' do
    stub_request(:get, 'https://trello_api.com/boards/a_board_id/cards?key=a_key&token=a_token&fields=desc%2Cdue%2Cname%2Clabels&checklists=all')
      .to_return(status: 200, body: JSON.dump(board))

    response = @trello.board('a_board_id')

    expect(response).to eq(board)
  end

  def board
    [{
      id: '5ab3fb08900b981e8ebd7d7',
      desc: 'Santa Coloma de Queralt',
      due: nil,
      name: 'localization 2',
      labels: [],
      checklists: []
    }, {
      id: '5ab3fb08900b981e8ebd7d7',
      desc: 'Santa Coloma de Queralt',
      due: nil,
      name: 'localization 2',
      labels: [],
      checklists: []
    }]
  end

  def card
    {
      id: '5ab3fb08900b981e8ebd7d7',
      desc: 'Santa Coloma de Queralt',
      due: nil,
      name: 'localization 2',
      labels: [],
      checklists: []
    }
  end
end
