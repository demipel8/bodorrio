# frozen_string_literal: true

require 'external/random_gif'

RSpec.describe 'random gif' do
  it 'gets a random gif' do
    ENV['RANDOM_GIF_URL'] = 'https://get_random_gif.com'
    ENV['RANDOM_GIF_TAG'] = 'a_tag'

    stub_request(:get, 'https://get_random_gif.com?tag=a_tag')
      .to_return(status: 200, body: '{ "image_url": "a_gif_url" }')

    response = RandomGif.new.get

    expect(response).to eq('a_gif_url')
  end
end
