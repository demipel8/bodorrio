# frozen_string_literal: true

require 'services/invitees/repository'
require 'services/trello_board'

require_relative '../../../fixtures'

RSpec.describe 'invitees repository' do
  it 'gets the lunch invitees list' do
    ENV['TRELLO_LUNCH_CARD'] = 'lunch_card_id'

    repository = InviteesRepository.new(TrelloBoardTest.new)

    expect(repository.lunch_list).to eq(lunch_list)
  end

  it 'gets the party invitees list' do
    ENV['TRELLO_PARTY_CARD'] = 'party_card_id'

    repository = InviteesRepository.new(TrelloBoardTest.new)

    expect(repository.party_list).to eq(party_list)
  end

  def lunch_list
    %w[a_familiar another_familiar a_familiar_from_another_list]
  end

  def party_list
    %w[a_friend another_friend]
  end

  class TrelloBoardTest < TrelloBoard
    def initialize() end

    def invitees
      [
        Card.new('lunch', [{ members: %w[a_familiar another_familiar] }, { members: %w[a_familiar_from_another_list] }]),
        Card.new('party', [{ members: %w[a_friend another_friend] }])
      ]
    end

    class Card
      attr_reader :name, :checklists

      def initialize(name, checklists)
        @name = name
        @checklists = checklists
      end
    end
  end
end
