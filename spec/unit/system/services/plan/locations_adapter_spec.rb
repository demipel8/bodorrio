# frozen_string_literal: true

require 'services/plan/locations_adapter'
require 'services/plan/trello_card'
require 'external/google_maps_embed'

RSpec.describe 'Localizations adapter' do
  it 'gets a list of localizations' do
    adapter = LocationsAdapter.new(locations_cards, GoogleMapsEmbedTest)

    expect(adapter.list).to eq(locations)
  end

  def locations_cards
    [
      LocationTrelloCardTest.new,
      LocationTrelloCardTest.new
    ]
  end

  def locations
    [location, location]
  end

  def location
    {
      name: 'bariloche',
      google_maps_embed_url: 'an_url'
    }
  end

  class LocationTrelloCardTest < TrelloCard
    def initialize() end

    def description
      'bariloche'
    end
  end

  class GoogleMapsEmbedTest < GoogleMapsEmbed
    def url_for(_)
      'an_url'
    end
  end
end
