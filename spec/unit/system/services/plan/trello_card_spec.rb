# frozen_string_literal: true

require 'services/plan/trello_card'
require 'external/trello'
require 'time'

require_relative '../../../fixtures'

RSpec.describe TrelloCard do
  let(:raw_card) { raw_card_data }

  it 'get a trello card info given an id' do
    card = TrelloCard.from_id('a_card_id', CardTrelloTest.new)

    expect(card.name).to eq('name')
    expect(card.description).to eq('Santa Coloma de Queralt')
    expect(card.date).to eq(Time.parse('2018-06-08T11:30:00.000Z'))
    expect(card.labels).to eq([])
    expect(card.checklists).to eq(
      [{
        name: 'rum',
        members: %w[A_friend Lol A_different_member]
      }, {
        name: 'gin',
        members: ['A_family_member']
      }, {
        name: 'vodka',
        members: []
      }]
    )
  end

  it 'get a trello card info from raw data' do
    card = TrelloCard.new(FIXTURES.raw_card)

    expect(card.name).to eq('name')
    expect(card.description).to eq('Santa Coloma de Queralt')
    expect(card.date).to eq(Time.parse('2018-06-08T11:30:00.000Z'))
    expect(card.labels).to eq([])
    expect(card.checklists).to eq(
      [{
        name: 'rum',
        members: %w[A_friend Lol A_different_member]
      }, {
        name: 'gin',
        members: ['A_family_member']
      }, {
        name: 'vodka',
        members: []
      }]
    )
  end

  class CardTrelloTest < Trello
    def card(_)
      FIXTURES.raw_card
    end
  end
end
