# frozen_string_literal: true

require 'services/plan/events_adapter'
require 'services/plan/trello_card'

require_relative '../../../fixtures'

RSpec.describe 'events adapter' do
  it 'adapts a list of events cards' do
    adapter = EventsAdapter.new(cards_events)

    expect(adapter.list).to eq(events)
  end

  def cards_events
    [
      EventsTrelloCardTest.new,
      EventsTrelloCardTest.new,
      EventsTrelloCardTest.new
    ]
  end

  def events
    [event, event, event]
  end

  def event
    {
      name: 'a name',
      description: "it is comming\n yes it is",
      hour: '11:30',
      time: '2018-06-08 11:30:00 UTC',
      labels: %w[lunch party]
    }
  end

  class EventsTrelloCardTest < TrelloCard
    def initialize() end

    def name
      'a name'
    end

    def description
      "it is comming\n yes it is"
    end

    def date
      Time.parse('2018-06-08T11:30:00.000Z')
    end

    def labels
      [
        {
          id: '8g3yf39ryfg93f',
          idBoard: 'ob3jbf3uirfb93',
          name: 'lunch',
          color: 'green',
          uses: 3
        },
        {
          id: 'fh9b3ub3rv3',
          idBoard: 'ou3efb3oebf3ef',
          name: 'party',
          color: 'yellow',
          uses: 2
        }
      ]
    end
  end
end
