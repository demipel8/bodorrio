# frozen_string_literal: true

require 'services/plan/gif_repository'
require 'external/random_gif'

require_relative '../../../fixtures'

RSpec.describe 'gif repository' do
  it 'retrieves gifs' do
    repository = GifRepository.new(RandomGifTest)

    expect(repository.get).to eq(event)
  end

  def event
    {
      gif: 'a_gif_url'
    }
  end

  class RandomGifTest < RandomGif
    def get
      'a_gif_url'
    end
  end
end
