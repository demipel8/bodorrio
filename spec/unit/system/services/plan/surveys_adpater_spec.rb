# frozen_string_literal: true

require 'services/plan/surveys_adapter'
require 'services/plan/trello_card'

RSpec.describe 'surveys adapter' do
  it 'gets a user surveys' do
    survey_adapter = SurveysAdapter.new(surveys_cards)
    expect(survey_adapter.get('a_user')).to eq(surveys)
  end

  def surveys_cards
    [SurveyTrelloCardTest.new]
  end

  def surveys
    [
      {
        id: 'survey_card_id',
        name: 'drink survey',
        question: 'What do you drink?',
        options: {
          rum: true,
          gin: false,
          vodka: false,
          other: true
        }
      }
    ]
  end

  class SurveyTrelloCardTest < TrelloCard
    def initialize() end

    def id
      'survey_card_id'
    end

    def name
      'drink survey'
    end

    def description
      'What do you drink?'
    end

    def checklists
      [{
        name: 'rum',
        members: ['A_user']
      }, {
        name: 'gin',
        members: []
      }, {
        name: 'vodka',
        members: []
      }, {
        name: 'other',
        members: ['A_user']
      }]
    end
  end
end
