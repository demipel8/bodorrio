# bodorrio
My wedding web

This project makes use of the [Trello rest API](https://developers.trello.com/reference/)

## Build the project

*It requires docker and docker-compose*

```
make build
```

## Test the project

```
make test
```

## Lint the project

```
make lint
```

## Run the project

Execute the following shell command:

```
make run
```

Open a browser and go to `http://localhost:4000`

Example Trello board: https://trello.com/b/uNEr4dgX/bodorriotest

### Build your own

It requires a Trello Account and a Google Maps Embed API key

Edit 'spec/integration/.env' with your environment variables.

Create your Trello board with `infrastructure/create_trello_board.sh <Trello_board_name>`

*It requires you to set TRELLO_KEY and TRELLO_TOKEN env variables before running*

### Reflections

As in a lot of projects this one is using generalization before it
is needed. Making it hard to change, as the wedding invitations are
being handled, so in an effort to go faster we are going to keep the
design less generic.

What we want to see in the we is:

- Welcome Card (Header)
  - Welcome message (with name of the user)
  - Logout button
- Schedule
 - Mix Events and location components
- Surveys

Some cards will be hardcoded first on the client and later migrated to Trello

#### Possible next steps
 - [x] Logging to Sumologic (require company mail)
 - [ ] Dockerignore
 - [ ] Use alpine images
 - [ ] Terraform infra
 - [ ] Check Puma logger
 - [ ] Error management on client
 - [ ] Rethink styling strategy
 - [ ] https://github.com/mobxjs/mobx
 - [ ] React Components spec
  - https://blog.kentcdodds.com/testing-%EF%B8%8F-components-using-render-props-5623ab1814c
 - [ ] smoke suite?
 - [ ] acceptance suite
 - [ ] Relate Events with localizations in Trello
