#!/usr/bin/env sh

build_client_app() {
  docker build client/ -t build-client-app
  docker run --rm -v `pwd`/client/build:/code/build build-client-app npm run build

  rm -rf public
  cp -r client/build/ public
}

build_image() {
  docker build --file heroku/Dockerfile --pull -t "$REGISTRY_IMAGE" .
}

login_to_registry() {
  docker login --username=_ --password=$REGISTRY_PASSWORD $REGISTRY_URL
}

push_image_to_registry() {
  docker push "$REGISTRY_IMAGE"
}

build_client_app
build_image
login_to_registry
push_image_to_registry
