#!/usr/bin/env sh

set -x

trap abort SIGSEGV SIGTERM

cleanup() {
  docker-compose --file docker-compose.test.yml down --rmi local --timeout 30
}

abort() {
  cleanup
  exit 1
}

docker-compose -f docker-compose.test.yml build

docker-compose --file docker-compose.test.yml run --rm bodorrio-test-server bundle exec rspec spec/unit
ok=$?

docker-compose --file docker-compose.test.yml run --rm bodorrio-integration-test-server bundle exec rspec spec/integration
ok=$(($ok + $?))

docker-compose --file docker-compose.test.yml run --rm bodorrio-test-server bundle exec rspec spec/linting
ok=$(($ok + $?))

cleanup

exit $ok
