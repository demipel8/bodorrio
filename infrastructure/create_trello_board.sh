#!/usr/bin/env sh

name=$1

curl --request POST \
  --url "https://api.trello.com/1/boards/?key=$TRELLO_KEY&token=$TRELLO_TOKEN&name=$name&desc=Created%20by%20BodorrioApp&idBoardSource=5ad0ae934e93e3d71191fbcd&keepFromSource=none&prefs_permissionLevel=private&prefs_voting=disabled&prefs_comments=members&prefs_invitations=members&prefs_selfJoin=true&prefs_cardCovers=true&prefs_background=purple&prefs_cardAging=regular"
