# frozen_string_literal: true

require 'newrelic_rpm'
require_relative 'system/server.rb'

map '/' do
  run System::Server
end
