export default (key) => {
  const store = (value) => {
    localStorage.setItem(key, value)
  }

  const get = () => {
    return localStorage.getItem(key)
  }

  const remove = () => {
    localStorage.removeItem(key)
  }

  return {
    store,
    get,
    remove
  }
}
