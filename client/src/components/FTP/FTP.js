import React,{ Component } from 'react'
import { Segment, Header, Image } from 'semantic-ui-react'

class FTP extends Component {
  render() {
    return (
      <Segment>
        <Header as='h1'>{ this.props.message }</Header>
        <Image src={ this.props.gif } />
      </Segment>
    )
  }
}

export default FTP
