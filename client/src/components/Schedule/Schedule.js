import React,{ Component } from 'react'
import Moment from 'moment'
import { Segment, Header, Image } from 'semantic-ui-react'
import { Localization } from 'components'
import * as styles from './Schedule.less'

class Schedule extends Component {
  state = {
    events: this.props.events,
    localizations: this.props.localizations
  }

  formatDate = (raw_date) => Moment(new Date(raw_date)).format('DD/MM HH:mm')

  event(data, index) {
    return (
      <div key={ index } className={ styles.schedule }>
        <p className={ styles.time }>- { this.formatDate(data.time) }</p>
        <div>
          <p className={ styles.title }>{ data.name }</p>
          <p className={ styles.event_description }>{ data.description }</p>
        </div>
      </div>
    )
  }

  localization(data, index) {
    return (
      <Localization key={ index } name={ data.name } google_maps_embed_url={ data.google_maps_embed_url } />
    )
  }

  render() {
    const events = this.state.events.slice()
    const localizations = this.state.localizations.slice()

    return (
      <div className={ styles.card }>
        <Header as='h2'>El Planazo</Header>
        <Segment className={ styles.schedule_in_a_location } vertical>
          <div className={ styles.timetable }>
            { this.event(events.shift().data, 0) }
          </div>
          <div className={ styles.map }>
            { this.localization(localizations.shift().data, 0) }
          </div>
        </Segment>
        <Segment className={ styles.schedule_in_a_location } vertical>
          <div className={ styles.timetable }>
            { events.map((event, index) => this.event(event.data, index)) }
          </div>
          <div className={ styles.map }>
            { localizations.map((localization, index) => this.localization(localization.data, index)) }
          </div>
        </Segment>
      </div>
    )
  }
}

export default Schedule
