import React,{ Component } from 'react'
import { Segment, Header, Button } from 'semantic-ui-react'
import * as styles from './Welcome.less'

class Welcome extends Component {
  render() {
    return (
      <div className={ styles.card }>
        <div className={ styles.card_header }>
        <Header as='h2' className={ styles.name } >Hola { this.props.name }</Header>
        <Button className={ styles.relogin } onClick={ this.props.callback }>Cambiar de invitado</Button>
        </div>
        <p>Esperamos que puedas compartir este fin de semana con nosotros</p>
      </div>
    )
  }
}

export default Welcome
