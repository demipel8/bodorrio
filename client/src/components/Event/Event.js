import React,{ Component } from 'react'
import { Header, Segment } from 'semantic-ui-react'
import * as styles from './Event.less'

class Event extends Component {
  localize_time_to_UTC_plus_two() {
    const hour = this.props.hour
    const chars = hour.split(':')

    chars[0] = parseInt(chars[0]) + 2

    return chars.join(':')
  }

  render() {
    return (
      <Segment className={ styles.card }>
        <Header as='h1' className={ styles.capitalize }>{ this.props.title }</Header>
        <Header as='h4'>{ this.localize_time_to_UTC_plus_two() }</Header>
        <p className={ styles.preserve_lines }>{ this.props.description }</p>
      </Segment>
    )
  }
}

export default Event
