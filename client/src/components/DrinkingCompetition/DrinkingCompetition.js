import React,{ Component } from 'react'
import { Progress, Header, Segment } from 'semantic-ui-react'
import * as styles from './DrinkingCompetition.less'

class DrinkingCompetition extends Component {
  colors = (index) => ['green', 'brown'][index % 2]

  options = () => this.props.competition.data.options.map((option, index) => {
    return (<Progress key={ index } value={ option.done } total={ option.total } progress='ratio' color={ this.colors(index) }>{ option.name }</Progress>)
  })

  render() {
    return (
      <Segment className={ styles.card }>
        <Header as='h1' className={ styles.capitalize }>Botellas acabadas</Header>
        { this.options() }
      </Segment>
    )
  }
}

export default DrinkingCompetition
