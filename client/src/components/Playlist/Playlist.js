import React,{ Component } from 'react'
import { Embed, Segment } from 'semantic-ui-react'
import * as styles from './Playlist.less'

class Localization extends Component {
  render() {
    return (
      <div className={styles.card}>
          <Embed className={styles.iframe}
            url='https://populrr.com/party/bodorrio'
            active={true}
            iframe={{
             allowFullScreen: true,
             scrolling:"yes",
             style: {
               padding: 10
             }
           }}
          />
      </div>
    )
  }
}

export default Localization
