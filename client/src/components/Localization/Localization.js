import React,{ Component } from 'react'
import { Embed, Segment } from 'semantic-ui-react'
import * as styles from './Localization.less'

class Localization extends Component {
  render() {
    return (
      <div className={styles.card}>
          <Embed
            url={ this.props.google_maps_embed_url }
            active={true}
          />
      </div>
    )
  }
}

export default Localization
