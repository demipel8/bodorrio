import React,{ Component } from 'react'
import { Segment, Header, Checkbox, List } from 'semantic-ui-react'
import * as styles from './Survey.less'

class Survey extends Component {
  handleOnChange = (event, data) => this.props.callback(this.props.id, data.label, data.checked)

  render() {
    return (
      <div className={ styles.card }>
        <Header as='h1'>{ this.props.question }</Header>
        <List horizontal>
          {
            Object.keys(this.props.options).map((key, index) => {
              return (
                <List.Item key={ index } className={ styles.list_item }>
                  <Checkbox label={ key } defaultChecked={ this.props.options[key] } onChange={ this.handleOnChange } />
                </List.Item>
              )
            })
          }
        </List>
      </div>
    )
  }
}

export default Survey
