import React,{ Component } from 'react'
import { Survey } from 'components'
import * as styles from './SurveySection.less'

class SurveySection extends Component {
  render() {
    return (
      <div className={ styles.card }>
        {
          this.props.surveys.map((survey, index) => {
            return (<Survey key={ index } id={ survey.data.id } question={ survey.data.question } options={ survey.data.options } callback={ this.props.updateSurvey }></Survey>)
          })
        }
        <p className={ styles.footnote }>Las respuestas de las encuestas se guardan automáticamente</p>
    </div>
    )
  }
}

export default SurveySection
