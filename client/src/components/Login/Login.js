import React,{ Component } from 'react'
import {render} from 'react-dom';
import { Button, Icon, Input, Header, Card, Image } from 'semantic-ui-react'
import * as styles from './Login.less'
import shades from 'resources/shades.svg'

class Login extends Component {
  ENTER_KEY = 13

  state = {
    name: '',
    surnames: ''
  }

  handleOnBlur = (event) => this.updateStateFrom(event)
  handleOnClick = () => this.sendName()

  handleKeyPress = (event) => {
    if (event.keyCode != this.ENTER_KEY) return

    this.updateStateFrom(event)
    this.sendName()
  }

  updateStateFrom(event) {
    const {name, value} = event.target
    this.state[name] = value
  }

  sendName() {
    const fullName = `${this.state.name} ${this.state.surnames}`

    this.props.callback(fullName.trim())
  }

  render() {
    return (
      <div className={styles.card}>
        <Image src={ shades } className={styles.images} />
        <Input name="name" className={styles.input} onBlur={this.handleOnBlur} onKeyDown={this.handleKeyPress} placeholder='Nombre' autoFocus/>
        <Input name="surnames" className={styles.input} onBlur={this.handleOnBlur} onKeyDown={this.handleKeyPress} placeholder='Apellidos' />
        <Button onClick={this.handleOnClick} floated='right' animated>
          <Button.Content visible>Entrar</Button.Content>
          <Button.Content hidden>
            <Icon name='right arrow' />
          </Button.Content>
        </Button>
      </div>
    )
  }
}

Login.defaultProps = {
  callback: () => {}
};

export default Login
