import React,{ Component } from 'react'
import { Button } from 'semantic-ui-react'

class BackToLogin extends Component {
  handleOnClick = () => this.props.callback()

  render() {
    return (
      <Button floated='right' onClick={this.handleOnClick}>Cambiar de invitado</Button>
    )
  }
}

export default BackToLogin
