import React, { Component } from 'react'
import { LoginView, NotAnInviteeView, MainView } from 'views'
import { Loader } from 'semantic-ui-react'
import Storage from './services/storage'
import 'styling/semantic.less'
import 'styling/layout.less'

const NAME_STORAGE = Storage('bodorrio_name')
const NULL = 'null'

class App extends Component {
  state = {
    activeView: 'login',
    plan: '',
    name: NAME_STORAGE.get()
  }

  getInvitee = (name) => {
    NAME_STORAGE.store(name)
    this.setState({activeView: 'loading', name: name })

    fetch('/invitee', {
      method: 'POST',
      body: JSON.stringify({ name })
    })
      .then(response => response.json())
      .then(this.setPlan)
  }

  updateSurvey = (survey, option, checked) => {
    fetch('/survey', {
      method: 'POST',
      body: JSON.stringify({
        name: this.state.name,
        survey,
        option,
        checked
      })
    })
  }

  setPlan = (components) => {
    let nextView = 'notAnInvitee'

    if (components.length > 1) {
      nextView = 'plan'
    }

    this.setState({
      activeView: nextView,
      plan: components
    })
  }

  backToLogin = () => {
    NAME_STORAGE.remove()
    this.setState({activeView: 'login', name: NULL })
  }

  login = () => {
    if (this.state.name == null ||  this.state.name == NULL) return LoginView(this.getInvitee)

    this.getInvitee(this.state.name)
    return (<Loader active/>)
  }

  notAnInvitee = () => (
    <div id='plan_container'>
      { NotAnInviteeView(this.state.plan[0].data, this.backToLogin) }
    </div>
  )

  plan = () => (
    <div id='plan_container'>
      { MainView(this.state.plan, this.backToLogin, this.state.name, this.updateSurvey) }
    </div>
  )

  loading = () => (<Loader active/>)

  render() {
    const views = {
      'login': this.login,
      'notAnInvitee': this.notAnInvitee,
      'plan': this.plan,
      'loading': this.loading
    }

    return views[this.state.activeView]()
  }
}

export default App
