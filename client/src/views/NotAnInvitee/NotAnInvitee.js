import React from 'react'
import { FTP, BackToLogin } from 'components'

export default (data, backToLogin) => [
  (<FTP key='0' gif={ data.gif } message={ data.description }/>),
  (<BackToLogin key='1' callback={ backToLogin }></BackToLogin>)
]
