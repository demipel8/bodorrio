import React from 'react'
import { Login } from 'components'

export default (getInvitee) => (
  <Login callback={ getInvitee }></Login>
)
