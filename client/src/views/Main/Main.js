import React from 'react'
import Moment from 'moment'
import { Welcome, Schedule, SurveySection, Playlist, DrinkingCompetition } from 'components'
import { Embed, Segment } from 'semantic-ui-react'

export default (body, backToLogin, name, updateSurvey) => {
  const events = body.filter(component => component.component == 'event')
  const localizations = body.filter(component => component.component == 'localization')
  const surveys = body.filter(component => component.component == 'survey')
  const competitions = body.filter(component => component.component == 'competition')

  if(isPartyTime()) {
    events.shift()

    return [
      (<Welcome key='0' name={ name } callback={ backToLogin }></Welcome>),
      (<Schedule key='1' events={ events } localizations={ localizations }></Schedule>),
      (<DrinkingCompetition key='2' competition={ competitions[0] }/>),
      (<Playlist key='3' />)
    ]
  }
  return [
    (<Welcome key='0' name={ name } callback={ backToLogin }></Welcome>),
    (<Schedule key='1' events={ events } localizations={ localizations }></Schedule>),
    (<SurveySection key='2' updateSurvey={ updateSurvey } surveys={ surveys }></SurveySection>),
    (<Playlist key='3'/>)
  ]
}

function isPartyTime() {
  return Moment() > Moment('2018-06-08 14:00')
}
