build:
	@docker-compose build
	@docker-compose --file docker-compose.test.yml build

test: unit-tests integration-tests linting-tests

unit-tests:
	@docker-compose --file docker-compose.test.yml run --rm bodorrio-test-server bundle exec rspec spec/unit

integration-tests:
	@docker-compose --file docker-compose.test.yml run --rm bodorrio-integration-test-server bundle exec rspec spec/integration

linting-tests:
	@docker-compose --file docker-compose.test.yml run --rm bodorrio-test-server bundle exec rspec spec/linting

lint:
	@docker-compose --file docker-compose.test.yml run --rm bodorrio-test-server bundle exec rubocop

run:
	@docker-compose up

down:
	@docker-compose down --remove-orphans
	@docker-compose --file docker-compose.test.yml down --remove-orphans

build-client:
	@docker-compose run --rm bodorrio-app npm run build
	@rm -rf public
	@cp -r client/build/ public
