# frozen_string_literal: true

class CompetitionsAdapter
  def initialize(competitions_cards)
    @competitions_cards = competitions_cards
  end

  def list
    competitions_from_cards
  end

  private

  def competitions_from_cards
    @competitions_cards.map do |card|
      competition_from(card)
    end
  end

  def competition_from(card)
    {
      name: card.name,
      options: options_from_checklists(card)
    }
  end

  def options_from_checklists(card)
    checklists = card.checklists
    options = []

    checklists.each do |checklist|
      checklist[:members].each do |option|
        strings = option.split(':')
        name = strings[0]

        numbers = strings[1].split(' ')
        total = numbers[0]
        done = numbers[1]

        options << { name: name, total: total, done: done }
      end
    end

    options
  end
end
