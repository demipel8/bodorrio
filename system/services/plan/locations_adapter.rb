# frozen_string_literal: true

require_relative '../../external/google_maps_embed'

class LocationsAdapter
  def initialize(locations, google_maps_embed=GoogleMapsEmbed)
    @locations = locations
    @google_maps_embed = google_maps_embed.new
  end

  def list
    locations_from_cards
  end

  private

  def locations_from_cards
    @locations.map do |card|
      location_for(card.description)
    end
  end

  def location_for(name)
    {
      name: name,
      google_maps_embed_url: @google_maps_embed.url_for(name)
    }
  end
end
