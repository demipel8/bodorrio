# frozen_string_literal: true

require_relative '../../helpers/name'

class SurveysAdapter
  def initialize(surveys)
    @surveys = surveys
  end

  def get(user)
    surveys_for(user)
  end

  private

  def surveys_for(user)
    @surveys.map { |card| survey_from(card, user) }
  end

  def survey_from(card, user)
    {
      id: card.id,
      name: card.name,
      question: card.description,
      options: options_from_checklists(card, user)
    }
  end

  def options_from_checklists(card, user)
    checklists = card.checklists
    options = {}
    name = Helpers::Name.homogenize(user)

    checklists.each do |checklist|
      selected = checklist[:members].include?(name)

      options[checklist[:name].to_sym] = selected
    end

    options
  end
end
