# frozen_string_literal: true

require 'time'

class EventsAdapter
  def initialize(events_cards)
    @events_cards = events_cards
  end

  def list
    events_from_cards
  end

  private

  def events_from_cards
    @events_cards.map do |card|
      event_from(card)
    end
  end

  def event_from(card)
    {
      name: card.name,
      description: card.description,
      hour: hour_from(card.date),
      time: card.date.to_s,
      labels: extract_label_names(card.labels)
    }
  end

  def hour_from(time)
    time.strftime('%H:%M')
  end

  def extract_label_names(labels)
    labels.map { |label| label[:name] }
  end
end
