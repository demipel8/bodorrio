# frozen_string_literal: true

require 'time'
require_relative '../../external/trello'

class TrelloCard
  LAST_CHARACTER = -1
  WITHOUT_LAST_CHARACTER = 0...-1

  def self.from_id(id, trello_client=Trello.new)
    new(trello_client.card(id))
  end

  def initialize(card_data)
    @card = card_data
  end

  def id
    @card[:id]
  end

  def description
    remove_last_line_break(@card[:desc])
  end

  def name
    @card[:name]
  end

  def date
    Time.parse(@card[:due])
  end

  def labels
    @card[:labels]
  end

  def checklists
    checklists_from_card
  end

  private

  def remove_last_line_break(string)
    return string unless ends_with_linebreak?(string)
    string[WITHOUT_LAST_CHARACTER]
  end

  def ends_with_linebreak?(string)
    string[LAST_CHARACTER] == "\n"
  end

  def checklists_from_card
    raw_checklists = @card[:checklists]

    raw_checklists.map do |checklist|
      {
        name: checklist[:name],
        members: checklist[:checkItems].map { |item| item[:name] }.flatten
      }
    end
  end
end
