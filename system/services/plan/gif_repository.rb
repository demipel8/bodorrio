# frozen_string_literal: true

require_relative '../../external/random_gif'

class GifRepository
  def initialize(gif=RandomGif)
    @gif = gif.new
  end

  def get
    {
      gif: @gif.get
    }
  end
end
