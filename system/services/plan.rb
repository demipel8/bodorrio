# frozen_string_literal: true

require_relative 'invitees'
require_relative 'trello_board'
require_relative 'plan/locations_adapter'
require_relative 'plan/events_adapter'
require_relative 'plan/gif_repository'
require_relative 'plan/surveys_adapter'
require_relative 'plan/competitions_adapter'

class Plan
  def initialize(user)
    @user = user
    @gif = GifRepository.new

    @trello_board = TrelloBoard.new(ENV['TRELLO_BOARD'])
    @invitee = Invitees.new(user, @trello_board)
    @events = EventsAdapter.new(@trello_board.events)
    @locations = LocationsAdapter.new(@trello_board.locations)
    @surveys = SurveysAdapter.new(@trello_board.surveys)
    @competitions = CompetitionsAdapter.new(@trello_board.competitions)
  end

  def generate
    invitee_plan = not_an_invitee
    invitee_plan = lunch if @invitee.lunch?
    invitee_plan = party if @invitee.party?
    invitee_plan
  end

  def not_an_invitee
    description = @trello_board.not_an_invitee.description
    gif = @gif.get[:gif]

    [
      {
        component: 'not_an_invitee',
        data: {
          description: description,
          gif: gif
        }
      }
    ]
  end

  def lunch
    events_for('lunch') + locations + surveys + competitions
  end

  def party
    events_for('party') + locations + surveys + competitions
  end

  private

  def events_for(plan)
    plan_events = @events.list.select { |data| data[:labels].include?(plan) }

    plan_events.map do |data|
      {
        component: 'event',
        data: data
      }
    end
  end

  def locations
    @locations.list.map do |data|
      {
        component: 'localization',
        data: data
      }
    end
  end

  def surveys
    @surveys.get(@user).map do |data|
      {
        component: 'survey',
        data: data
      }
    end
  end

  def competitions
    @competitions.list.map do |data|
      {
        component: 'competition',
        data: data
      }
    end
  end
end
