# frozen_string_literal: true

require_relative '../external/trello'
require_relative 'plan/trello_card'

class TrelloBoard
  def initialize(id, trello_client=Trello, trello_card=TrelloCard)
    raw_board = trello_client.new.board(id)
    @cards = []

    raw_board.each do |card|
      @cards.push(trello_card.new(card))
    end
  end

  def invitees
    @invitees_cards ||= extract_invitees
  end

  def not_an_invitee
    @not_an_invitee_card ||= extract_not_an_invitee_card
  end

  def events
    @events ||= extract_events
  end

  def locations
    @locations ||= extract_locations
  end

  def surveys
    @extract_surveys ||= extract_surveys
  end

  def competitions
    @extract_competitions ||= extract_competitions
  end

  private

  def extract_label_names(labels)
    labels.map { |label| label[:name] }
  end

  def extract_invitees
    @cards.select do |card|
      labels = extract_label_names(card.labels)
      labels.include?('invitees')
    end
  end

  def extract_not_an_invitee_card
    cards = @cards.select do |card|
      labels = extract_label_names(card.labels)
      labels.include?('uninvited')
    end

    cards.first
  end

  def extract_events
    @cards.select do |card|
      labels = extract_label_names(card.labels)
      labels.include?('events')
    end
  end

  def extract_locations
    @cards.select do |card|
      labels = extract_label_names(card.labels)
      labels.include?('locations')
    end
  end

  def extract_surveys
    @cards.select do |card|
      labels = extract_label_names(card.labels)
      labels.include?('surveys')
    end
  end

  def extract_competitions
    @cards.select do |card|
      labels = extract_label_names(card.labels)
      labels.include?('competitions')
    end
  end
end
