# frozen_string_literal: true

require_relative '../../external/trello'
require_relative '../../helpers/name'

class SurveyOption
  def initialize(name, survey, option, trello=Trello)
    @trello = trello.new
    @name = Helpers::Name.homogenize(name)
    @checklist = checklist_for(option, survey)
  end

  def delete
    item = @checklist[:checkItems].select { |check_item| check_item[:name] == @name }.first

    return if item.nil?

    @trello.delete_item_from_checklists(@checklist[:id], item[:id])
  end

  def add
    @trello.add_item_to_checklists(@checklist[:id], @name)
  end

  private

  def checklist_for(option, survey)
    options_checklists = @trello.checklists_in_card(survey)
    options_checklists.select { |checklist| checklist[:name] == option }.first
  end
end
