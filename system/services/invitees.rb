# frozen_string_literal: true

require_relative 'invitees/repository'

class Invitees
  def initialize(name, trello_getaway, repository=InviteesRepository)
    @repository = repository.new(trello_getaway)
    @name = normalize_special_characters(name)
  end

  def lunch?
    lunch_invitees.any? { |invitee| compare_case_insensitive(invitee) }
  end

  def party?
    party_invitees.any? { |invitee| compare_case_insensitive(invitee) }
  end

  private

  def lunch_invitees
    @repository.lunch_list
  end

  def party_invitees
    @repository.party_list
  end

  def compare_case_insensitive(invitee)
    trimmed_name = trim_spaces(invitee)
    normalized_name = normalize_special_characters(trimmed_name)
    normalized_name.casecmp?(@name)
  end

  def normalize_special_characters(name)
    name.tr(
      'ÀÁÂÃÄÅàáâãäåĀāĂăĄąÇçĆćĈĉĊċČčÐðĎďĐđÈÉÊËèéêëĒēĔĕĖėĘęĚěĜĝĞğĠġĢģĤĥĦħÌÍÎÏìíîïĨĩĪīĬĭĮįİıĴĵĶķĸĹĺĻļĽľĿŀŁłŃńŅņŇňŉŊŋÒÓÔÕÖØòóôõöøŌōŎŏŐőŔŕŖŗŘřŚśŜŝŞşŠšſŢţŤťŦŧÙÚÛÜùúûüŨũŪūŬŭŮůŰűŲųŴŵÝýÿŶŷŸŹźŻżŽž',
      'AAAAAAaaaaaaAaAaAaCcCcCcCcCcDdDdDdEEEEeeeeEeEeEeEeEeGgGgGgGgHhHhIIIIiiiiIiIiIiIiIiJjKkkLlLlLlLlLlNnNnNnnNnOOOOOOooooooOoOoOoRrRrRrSsSsSsSssTtTtTtUUUUuuuuUuUuUuUuUuUuWwYyyYyYZzZzZz'
    )
  end

  def trim_spaces(name)
    name.strip
  end
end
