# frozen_string_literal: true

require_relative 'survey_update/survey_option'

class SurveyUpdate
  def initialize(name, survey, option, checked, survey_option=SurveyOption)
    @name = name
    @survey = survey
    @option = option
    @checked = checked
    @survey_option = survey_option.new(name, survey, option)
  end

  def do
    @survey_option.delete
    @survey_option.add if @checked
  end
end
