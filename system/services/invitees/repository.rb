# frozen_string_literal: true

class InviteesRepository
  def initialize(trello_getaway)
    @trello_getaway = trello_getaway
  end

  def lunch_list
    card = card_for('lunch')
    invitees_from(card.checklists)
  end

  def party_list
    card = card_for('party')
    invitees_from(card.checklists)
  end

  private

  def invitees_from(lists)
    lists.map { |list| list[:members] }.flatten
  end

  def card_for(list)
    @trello_getaway.invitees.select { |card| card.name == list }.first
  end
end
