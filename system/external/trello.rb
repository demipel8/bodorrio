# frozen_string_literal: true

require 'uri'
require 'net/http'
require 'json'
require 'openssl'

class Trello
  def initialize
    @key = ENV['TRELLO_KEY']
    @token = ENV['TRELLO_TOKEN']
  end

  def checklists_in_card(id)
    url = "#{url_for_card_field('checklists', id)}&checkItem_fields=name&fields=checkItems%2Cname"

    get_from(url)
  end

  def description_in_card(id)
    url = url_for_card_field('desc', id)

    get_from(url)
  end

  def due_date_in_card(id)
    url = url_for_card_field('due', id)

    get_from(url)
  end

  def name_in_card(id)
    url = url_for_card_field('name', id)

    get_from(url)
  end

  def labels_in_card(id)
    url = url_for_card_field('labels', id)

    get_from(url)
  end

  def add_item_to_checklists(checklist_id, name)
    encoded_name = URI.encode_www_form([['name', name]])
    url = "#{ENV['TRELLO_BASE_URL']}/checklists/#{checklist_id}/checkItems?#{encoded_name}&#{auth}"

    request(url, Net::HTTP::Post)
  end

  def delete_item_from_checklists(checklist_id, item_id)
    url = "#{ENV['TRELLO_BASE_URL']}/checklists/#{checklist_id}/checkItems/#{item_id}?#{auth}"

    request(url, Net::HTTP::Delete)
  end

  def card(id)
    url = "#{ENV['TRELLO_BASE_URL']}/cards/#{id}?fields=desc%2Cdue%2Cname%2Clabels&attachments=false&checklists=all&checklist_fields=name&#{auth}"

    get_from(url)
  end

  def board(id)
    url = "#{ENV['TRELLO_BASE_URL']}/boards/#{id}/cards?fields=desc%2Cdue%2Cname%2Clabels&checklists=all&#{auth}"

    get_from(url)
  end

  private

  def url_for_card_field(field, id)
    "#{ENV['TRELLO_BASE_URL']}/cards/#{id}/#{field}?#{auth}"
  end

  def auth
    URI.encode_www_form([['key', @key], ['token', @token]])
  end

  def get_from(url)
    response = request(url)

    parse(response)
  end

  def parse(response)
    JSON.parse(response, symbolize_names: true)
  end

  def request(url, method=Net::HTTP::Get)
    uri = URI(url)

    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    request = method.new(uri)

    response = http.request(request)
    response.read_body
  end
end
