# frozen_string_literal: true

class GoogleMapsEmbed
  MODE = 'place'

  def url_for(place)
    "#{ENV['GOOGLE_MAPS_EMBED_BASE_URL']}/#{MODE}?key=#{ENV['GOOGLE_MAPS_EMBED_API_KEY']}&q=#{formate(place)}"
  end

  private

  def formate(place)
    place.tr(' ', '+')
  end
end
