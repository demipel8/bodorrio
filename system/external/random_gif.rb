# frozen_string_literal: true

require 'uri'
require 'net/http'
require 'json'

class RandomGif
  def initialize
    @url = "#{ENV['RANDOM_GIF_URL']}?tag=#{ENV['RANDOM_GIF_TAG']}"
  end

  def get
    parse(request)[:image_url]
  end

  private

  def parse(response)
    JSON.parse(response, symbolize_names: true)
  end

  def request
    uri = URI(@url)

    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    request = Net::HTTP::Get.new(uri)

    response = http.request(request)
    response.read_body
  end
end
