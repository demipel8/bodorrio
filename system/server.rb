# frozen_string_literal: true

require 'sinatra/base'
require 'json'

require_relative 'services/plan'
require_relative 'services/survey_update'
require_relative 'helpers/logger'

module System
  class Server < Sinatra::Base
    project_root = File.join(root, '../public/')
    set :public_folder, project_root

    ['/'].each do |path|
      get path do
        File.read(File.join(project_root, 'index.html'))
      end
    end

    post '/invitee' do
      content_type :json

      parameters = JSON.parse(request.body.read, symbolize_names: true)
      name = parameters[:name]

      Helpers::Logger.instance.info("#{name} has requested a plan")

      Plan.new(name).generate.to_json
    end

    post '/survey' do
      content_type :json

      parameters = JSON.parse(request.body.read, symbolize_names: true)
      name = parameters[:name]
      survey = parameters[:survey]
      option = parameters[:option]
      checked = parameters[:checked]

      SurveyUpdate.new(name, survey, option, checked).do

      status 200
      {}.to_json
    end
  end
end
