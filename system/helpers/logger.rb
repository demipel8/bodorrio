# frozen_string_literal: true

require 'logger'

module Helpers
  class Logger
    def self.instance
      logger = ::Logger.new(STDOUT, level: :info)
      original_formatter = ::Logger::Formatter.new
      logger.formatter = proc { |severity, datetime, progname, msg|
        original_formatter.call(severity, datetime, progname, msg.dump)
      }
      logger
    end
  end
end
